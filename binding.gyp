{
  "targets": [
    {
      "target_name": "md4c_napi",
      "cflags": [ "-fno-exceptions", "-Wall", "-Wextra" ],
      "sources": [
        "lib/lib.cpp",
        "lib/constants.cpp",
        "lib/parse.cpp",
        "lib/html.cpp",
        "lib/3rdparty/md4c/md4c.c",
        "lib/3rdparty/md4c/md4c-html.c",
        "lib/3rdparty/md4c/entity.c"
      ],
      "include_dirs": [
        "<!@(node -p \"require('node-addon-api').include\")",
        "lib/3rdparty/md4c/"
      ],
      "defines": [ "NAPI_DISABLE_CPP_EXCEPTIONS", "NODE_ADDON_API_ENABLE_MAYBE", "MD4C_USE_UTF16", "NAPI_VERSION=8"],
    }
  ]
}
