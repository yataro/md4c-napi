#pragma once



#define CHECK_RETURN(cond, env, msg)															\
do {																							\
	if (cond) {																					\
		Napi::TypeError::New(env, msg).ThrowAsJavaScriptException();							\
																								\
		return;																					\
	}																							\
} while (false)

#define CHECK_TYPE_RETURN(type, value, tag, env)												\
do {																							\
	if (!(value).Is##type()) {																	\
		Napi::TypeError::New(env, tag " must be " #type ".").ThrowAsJavaScriptException();	\
																								\
		return;																					\
	} 																							\
} while(false)

#define UNWRAP_RETURN(maybe, value)																\
do {																							\
	if (!(maybe).UnwrapTo(value)) {																\
		return;																					\
	}																							\
} while(false)
