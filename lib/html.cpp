#include "html.hpp"
#include "utils.hpp"

#include <md4c-html.h>



namespace MD4CNapi {

struct HtmlData {
	Napi::Env env;

	Napi::Function process_output;

	inline HtmlData() : env(nullptr) {}
};

void process_output_callback(const MD_CHAR *text, MD_SIZE size, void *userdata) {
	auto *data = static_cast<HtmlData *>(userdata);

	data->process_output.Call({
		Napi::String::New(data->env, text, size)
	});

	// Exception can't be handled properly here...
}

void Html(const Napi::CallbackInfo& info){
	auto env = info.Env();

	CHECK_RETURN(info.Length() != 2, env, "Invalid number of arguments.");

	auto input_arg = info[0];
	auto options_arg = info[1];

	CHECK_TYPE_RETURN(String, input_arg, "input", env);
	CHECK_TYPE_RETURN(Object, options_arg, "options", env);

	auto options_obj = options_arg.As<Napi::Object>();

	HtmlData data;

	data.env = env;

	auto parser_flags = options_obj.Get("parser_flags");
	Napi::Number parser_flags_num;
	UNWRAP_RETURN(parser_flags, &parser_flags_num);
	CHECK_TYPE_RETURN(Number, parser_flags_num, "options.parser_flags", env);

	auto renderer_flags = options_obj.Get("renderer_flags");
	Napi::Number renderer_flags_num;
	UNWRAP_RETURN(renderer_flags, &renderer_flags_num);
	CHECK_TYPE_RETURN(Number, renderer_flags_num, "options.renderer_flags", env);

	auto process_output = options_obj.Get("process_output");
	UNWRAP_RETURN(process_output, &data.process_output);
	CHECK_TYPE_RETURN(Function, data.process_output, "options.process_output", env);

	auto input_str = input_arg.As<Napi::String>().Utf16Value();

	auto ret = md_html(
		input_str.data(),
		input_str.size(),
		&process_output_callback,
		&data,
		parser_flags_num.Uint32Value(),
		renderer_flags_num.Uint32Value()
	);

	CHECK_RETURN(ret != 0 && !env.IsExceptionPending(), env, "Render error.");
}

}
