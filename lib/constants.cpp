#include "constants.hpp"

#include <md4c.h>
#include <md4c-html.h>



namespace MD4CNapi {

#define SET_CONSTANT(obj, name, type)													\
do {																					\
	auto maybe = obj.Set(#name, Napi::Number::New(env, (uint32_t)(MD##type##_##name)));	\
	maybe.Check();																		\
} while(false)

#define SET_FLAG(f) SET_CONSTANT(flag, f, _FLAG)
#define SET_DIALECT(d) SET_CONSTANT(flag, DIALECT_##d, )
#define SET_BLOCK(b) SET_CONSTANT(block, b, _BLOCK)
#define SET_SPAN(s) SET_CONSTANT(span, s, _SPAN)
#define SET_TEXT(t) SET_CONSTANT(text, t, _TEXT)
#define SET_ALIGN(a) SET_CONSTANT(align, a, _ALIGN)
#define SET_HTML_FLAG(h) SET_CONSTANT(html_flag, h, _HTML_FLAG)

Napi::Object CreateConstantsObject(Napi::Env env) {
	auto constants = Napi::Object::New(env);

	auto flag = Napi::Object::New(env);
	auto block = Napi::Object::New(env);
	auto span = Napi::Object::New(env);
	auto text = Napi::Object::New(env);
	auto align = Napi::Object::New(env);
	auto html_flag = Napi::Object::New(env);

	SET_FLAG(COLLAPSEWHITESPACE);
	SET_FLAG(PERMISSIVEATXHEADERS);
	SET_FLAG(PERMISSIVEURLAUTOLINKS);
	SET_FLAG(PERMISSIVEEMAILAUTOLINKS);
	SET_FLAG(NOINDENTEDCODEBLOCKS);
	SET_FLAG(NOHTMLBLOCKS);
	SET_FLAG(NOHTMLSPANS);
	SET_FLAG(TABLES);
	SET_FLAG(STRIKETHROUGH);
	SET_FLAG(PERMISSIVEWWWAUTOLINKS);
	SET_FLAG(TASKLISTS);
	SET_FLAG(LATEXMATHSPANS);
	SET_FLAG(WIKILINKS);
	SET_FLAG(UNDERLINE);

	SET_FLAG(PERMISSIVEAUTOLINKS);
	SET_FLAG(NOHTML);

	SET_DIALECT(COMMONMARK);
	SET_DIALECT(GITHUB);

	SET_BLOCK(DOC);
	SET_BLOCK(QUOTE);
	SET_BLOCK(UL);
	SET_BLOCK(OL);
	SET_BLOCK(LI);
	SET_BLOCK(HR);
	SET_BLOCK(H);
	SET_BLOCK(CODE);
	SET_BLOCK(HTML);
	SET_BLOCK(P);
	SET_BLOCK(TABLE);
	SET_BLOCK(THEAD);
	SET_BLOCK(TBODY);
	SET_BLOCK(TR);
	SET_BLOCK(TH);
	SET_BLOCK(TD);

	SET_SPAN(EM);
	SET_SPAN(STRONG);
	SET_SPAN(A);
	SET_SPAN(IMG);
	SET_SPAN(CODE);
	SET_SPAN(DEL);
	SET_SPAN(LATEXMATH);
	SET_SPAN(LATEXMATH_DISPLAY);
	SET_SPAN(WIKILINK);
	SET_SPAN(U);

	SET_TEXT(NORMAL);
	SET_TEXT(NULLCHAR);
	SET_TEXT(BR);
	SET_TEXT(SOFTBR);
	SET_TEXT(ENTITY);
	SET_TEXT(CODE);
	SET_TEXT(HTML);
	SET_TEXT(LATEXMATH);

	SET_ALIGN(DEFAULT);
	SET_ALIGN(LEFT);
	SET_ALIGN(CENTER);
	SET_ALIGN(RIGHT);

	SET_HTML_FLAG(DEBUG);
	SET_HTML_FLAG(VERBATIM_ENTITIES);
	// SET_HTML_FLAG(SKIP_UTF8_BOM); Not needed.
	SET_HTML_FLAG(XHTML);

	constants.Set("flag", flag);
	constants.Set("block", block);
	constants.Set("span", span);
	constants.Set("text", text);
	constants.Set("align", align);
	constants.Set("html_flag", html_flag);

	return constants;
}

}
