/**
 * Flags specifying extensions/deviations from CommonMark specification.
 * By default (when flags == 0), we follow CommonMark specification.
 * The following flags may allow some extensions or deviations from it.
 */
export type Flags = {
    /**
     * - In text.NORMAL, collapse non-trivial whitespace into single ' '
     */
    COLLAPSEWHITESPACE: number;
    /**
     * - Do not require space in ATX headers ( ###header )
     */
    PERMISSIVEATXHEADERS: number;
    /**
     * - Recognize URLs as autolinks even without '<', '>'
     */
    PERMISSIVEURLAUTOLINKS: number;
    /**
     * - Recognize e-mails as autolinks even without '<', '>' and 'mailto:'
     */
    PERMISSIVEEMAILAUTOLINKS: number;
    /**
     * - Disable indented code blocks. (Only fenced code works.)
     */
    NOINDENTEDCODEBLOCKS: number;
    /**
     * - Disable raw HTML blocks.
     */
    NOHTMLBLOCKS: number;
    /**
     * - Disable raw HTML (inline).
     */
    NOHTMLSPANS: number;
    /**
     * - Enable tables extension.
     */
    TABLES: number;
    /**
     * - Enable strikethrough extension.
     */
    STRIKETHROUGH: number;
    /**
     * - Enable WWW autolinks (even without any scheme prefix, if they begin with 'www.')
     */
    PERMISSIVEWWWAUTOLINKS: number;
    /**
     * - Enable task list extension.
     */
    TASKLISTS: number;
    /**
     * - Enable $ and $$ containing LaTeX equations.
     */
    LATEXMATHSPANS: number;
    /**
     * - Enable wiki links extension.
     */
    WIKILINKS: number;
    /**
     * - Enable underline extension (and disables '_' for normal emphasis).
     */
    UNDERLINE: number;
    /**
     * - PERMISSIVEEMAILAUTOLINKS | PERMISSIVEURLAUTOLINKS | PERMISSIVEWWWAUTOLINKS.
     */
    PERMISSIVEAUTOLINKS: number;
    /**
     * - NOHTMLBLOCKS | NOHTMLSPANS.
     */
    NOHTML: number;
    /**
     * - Commonmark dialect.
     */
    DIALECT_COMMONMARK: number;
    /**
     * - Github dialect.
     */
    DIALECT_GITHUB: number;
};
/**
 * Block type.
 */
export type BlockType = number;
/**
 * Block represents a part of document hierarchy structure like a paragraph or list item.
 */
export type BlockTypes = {
    /**
     * - BODY.../BODY.
     */
    DOC: BlockType;
    /**
     * - BLOCKQUOTE.../BLOCKQUOTE.
     */
    QUOTE: BlockType;
    /**
     * - UL.../UL Detail: {@link ULDetail }.
     */
    UL: BlockType;
    /**
     * - OL.../OL Detail: {@link OLDetail }.
     */
    OL: BlockType;
    /**
     * - LI.../LI Detail: {@link LIDetail }.
     */
    LI: BlockType;
    /**
     * - HR.
     */
    HR: BlockType;
    /**
     * - H1.../H1 (for levels up to 6) Detail: {@link HDetail }.
     */
    H: BlockType;
    /**
     * - PRECODE.../CODE/PRE
     * Note the text lines within code blocks are terminated with '\n' instead of explicit text.BR.
     * Detail: {@link CODEDetail }
     */
    CODE: BlockType;
    /**
     * - Raw HTML block. This itself does not correspond to any particular HTML
     * tag. The contents of it _is_ raw HTML source intended to be put
     * in verbatim form to the HTML output.
     */
    HTML: BlockType;
    /**
     * - P.../P.
     */
    P: BlockType;
    /**
     * - TABLE.../TABLE Detail: {@link TABLEDetail }.
     */
    TABLE: BlockType;
    /**
     * - THEAD.../THEAD.
     */
    THEAD: BlockType;
    /**
     * - TBODY.../TBODY.
     */
    TBODY: BlockType;
    /**
     * - TR.../TR.
     */
    TR: BlockType;
    /**
     * - TH.../TH Detail: {@link TDDetail }.
     */
    TH: BlockType;
    /**
     * - TD.../TD Detail: {@link TDDetail }.
     */
    TD: BlockType;
};
/**
 * Span type.
 */
export type SpanType = number;
/**
 * Span represents an in-line piece of a document which should be rendered with
 * the same font, color and other attributes. A sequence of spans forms a block
 * like paragraph or list item.
 */
export type SpanTypes = {
    /**
     * - EM.../EM.
     */
    EM: SpanType;
    /**
     * - STRONG.../STRONG.
     */
    STRONG: SpanType;
    /**
     * - A HREF="XXX".../A Detail: {@link ADetail }.
     */
    A: SpanType;
    /**
     * - IMG SRC="XXX".../A
     * Note: Image text can contain nested spans and even nested images.
     * If rendered into ALT attribute of HTML IMG tag, it's responsibility
     * of the parser to deal with it.
     * Detail: {@link IMGDetail }.
     */
    IMG: SpanType;
    /**
     * - CODE.../CODE
     */
    CODE: SpanType;
    /**
     * - DEL.../DEL
     * Note: Recognized only when flag.STRIKETHROUGH is enabled.
     */
    DEL: SpanType;
    /**
     * - For recognizing inline ($) equations
     * Note: Recognized only when flag.LATEXMATHSPANS is enabled.
     */
    LATEXMATH: SpanType;
    /**
     * - For recognizing display ($$) equations
     * Note: Recognized only when flag.LATEXMATHSPANS is enabled.
     */
    LATEXMATH_DISPLAY: SpanType;
    /**
     * - /* Wiki links
     * Note: Recognized only when flag.WIKILINKS is enabled.
     * Detail: {@link WIKILINKDetail }
     */
    WIKILINK: SpanType;
    /**
     * - U.../U
     * Note: Recognized only when flag.UNDERLINE is enabled.
     */
    U: SpanType;
};
/**
 * Text type.
 */
export type TextType = number;
/**
 * Text is the actual textual contents of span.
 */
export type TextTypes = {
    /**
     * - Normal text.
     */
    NORMAL: TextType;
    /**
     * - NULL character. CommonMark requires replacing NULL character with
     * the replacement char U+FFFD, so this allows caller to do that easily.
     */
    NULLCHAR: TextType;
    /**
     * - Line breaks.
     * Note these are not sent from blocks with verbatim output (block.CODE
     * or block.HTML). In such cases, '\n' is part of the text itself.
     * BR (hard break).
     */
    BR: TextType;
    /**
     * - '\n' in source text where it is not semantically meaningful (soft break).
     */
    SOFTBR: TextType;
    /**
     * - Entity.
     * (a) Named entity, e.g. &nbsp;
     * (Note MD4C does not have a list of known entities.
     * Anything matching the regexp /&[A-Za-z][A-Za-z0-9]{1,47};/ is
     * treated as a named entity.)
     * (b) Numerical entity, e.g. &#1234;
     * (c) Hexadecimal entity, e.g. &#x12AB;
     * As MD4C is mostly encoding agnostic, application gets the verbatim
     * entity text into the text_callback().
     */
    ENTITY: TextType;
    /**
     * - Text in a code block (inside block.CODE) or inlined code (`code`).
     * If it is inside block.CODE, it includes spaces for indentation and
     * '\n' for new lines. text.BR and text.SOFTBR are not sent for this
     * kind of text.
     */
    CODE: TextType;
    /**
     * - Text is a raw HTML. If it is contents of a raw HTML block (i.e. not
     * an inline raw HTML), then text.BR and text.SOFTBR are not used.
     * The text contains verbatim '\n' for the new lines.
     */
    HTML: TextType;
    /**
     * - Text is inside an equation. This is processed the same way as inlined code
     * spans (`code`).
     */
    LATEXMATH: TextType;
};
/**
 * Align type.
 */
export type AlignType = number;
/**
 * Alignment.
 */
export type AlignTypes = {
    /**
     * - When unspecified.
     */
    DEFAULT: AlignType;
    /**
     * - Align left.
     */
    LEFT: AlignType;
    /**
     * - Align center.
     */
    CENTER: AlignType;
    /**
     * - Align right.
     */
    RIGHT: AlignType;
};
/**
 * HTML converter flags.
 */
export type HTMLFlags = {
    /**
     * - If set, debug output from parsing is sent to stderr.
     */
    DEBUG: number;
    /**
     * - Render entities as is.
     */
    VERBATIM_ENTITIES: number;
    /**
     * - Follow XTML style e.g. BR / not BR.
     */
    XHTML: number;
};
/**
 * Constants.
 */
export type Constants = {
    /**
     * - Parser flags.
     */
    flag: Flags;
    /**
     * - Block types.
     */
    block: BlockTypes;
    /**
     * - Span types.
     */
    span: SpanTypes;
    /**
     * - Text types.
     */
    text: TextTypes;
    /**
     * - Align types.
     */
    align: AlignTypes;
    /**
     * - HTML flags.
     */
    html: HTMLFlags;
};
/**
 * Attribute item.
 */
export type AttributeItem = {
    /**
     * - Text type.
     */
    type: TextType;
    /**
     * - Text.
     */
    text: string;
};
/**
 * Attribute.
 * This wraps strings which are outside of a normal text flow and which are
 * propagated within various detailed structures, but which still may contain
 * string portions of different types like e.g. entities.
 *
 * So, for example, lets consider this image:
 *
 * ![image alt text](http://example.org/image.png 'foo &quot; bar')
 *
 * The image alt text is propagated as a normal text via the text()
 * callback. However, the image title ('foo &quot; bar') is propagated as
 * Attribute in IMGDetail.title.
 *
 * Then the attribute IMGDetail.title shall provide the following:
 * -- [0]: "foo "   text.NORMAL
 * -- [1]: "&quot;" text.ENTITY
 * -- [2]: " bar"   text.NORMAL
 *
 * Note that these invariants are always guaranteed:
 * -- Currently, only text.NORMAL, text.ENTITY, text.NULLCHAR
 *    substrings can appear. This could change only of the specification
 *    changes.
 */
export type Attribute = AttributeItem[];
/**
 * Detailed info for UL block.
 */
export type ULDetail = {
    /**
     * - True if tight list, false if loose.
     */
    is_tight: bool;
    /**
     * - Item bullet character in Markdown source of the list, e.g. '-', '+', '*'.
     */
    mark: string;
};
/**
 * Detailed info for OL block.
 */
export type OLDetail = {
    /**
     * - Start index of the ordered list.
     */
    start: number;
    /**
     * - True if tight list, false if loose.
     */
    is_tight: bool;
    /**
     * - Character delimiting the item marks in MarkDown source, e.g. '.' or ')'
     */
    mark_delimiter: string;
};
/**
 * Detailed info for UL block.
 */
export type LIDetail = {
    /**
     * - Can be non-zero only with flag.TASKLISTS.
     */
    is_task: bool;
    /**
     * - If is_task, then one of 'x', 'X' or ' '. Undefined otherwise.
     */
    task_mark: string | undefined;
    /**
     * - If is_task, then offset in the input of the char between '[' and ']'.
     */
    task_mark_offset: number;
};
/**
 * Detailed info for H block.
 */
export type HDetail = {
    /**
     * - Header level (1 - 6).
     */
    level: number;
};
/**
 * Detailed info for CODE block.
 */
export type CODEDetail = {
    /**
     * - Code info.
     */
    info: Attribute;
    /**
     * - Code language.
     */
    lang: Attribute;
    /**
     * - The character used for fenced code block; or zero for indented code block.
     */
    fence_char: string;
};
/**
 * Detailed info for TABLE block.
 */
export type TABLEDetail = {
    /**
     * - Count of columns in the table.
     */
    col_count: number;
    /**
     * - Count of rows in the table header (currently always 1).
     */
    head_row_count: number;
    /**
     * - Count of rows in the table body.
     */
    body_row_count: number;
};
/**
 * Detailed info for TD and TH block.
 */
export type TDDetail = {
    /**
     * - Table cell align.
     */
    align: AlignType;
};
/**
 * Detailed info for block.
 */
export type BlockDetail = null | ULDetail | OLDetail | LIDetail | HDetail | CODEDetail | TABLEDetail | TDDetail;
/**
 * Detailed info for A span.
 */
export type ADetail = {
    /**
     * - URL.
     */
    href: Attribute;
    /**
     * - Title.
     */
    title: Attribute;
};
/**
 * Detailed info for IMG span.
 */
export type IMGDetail = {
    /**
     * - Image source.
     */
    src: Attribute;
    /**
     * - Image title.
     */
    title: Attribute;
};
/**
 * Detailed info for WIKILINK span.
 */
export type WIKILINKDetail = {
    /**
     * - Wikilink target.
     */
    target: Attribute;
};
/**
 * Detailed info for span.
 */
export type SpanDetail = null | ADetail | IMGDetail | WIKILINKDetail;
/**
 * Block callback.
 */
export type BlockCallback = (type: BlockType, detail: BlockDetail) => void;
/**
 * Span callback.
 */
export type SpanCallback = (type: SpanType, detail: SpanDetail) => void;
/**
 * Text callback.
 */
export type TextCallback = (type: TextType, text: string) => void;
/**
 * Debug callback.
 */
export type DebugCallback = (message: string) => void;
/**
 * Markdown parser options.
 */
export type ParserOptions = {
    /**
     * - Parser {@link Flag }s OR-ed together.
     */
    flags: number;
    /**
     * - Enter block callback.
     */
    enter_block: BlockCallback;
    /**
     * - Leave block callback.
     */
    leave_block: BlockCallback;
    /**
     * - Enter span callback.
     */
    enter_span: SpanCallback;
    /**
     * - Leave span callback.
     */
    leave_span: SpanCallback;
    /**
     * - Text callback.
     */
    text: TextCallback;
    /**
     * - Debug callback. Optional.
     * If provided and something goes wrong, this function gets called.
     * This is intended for debugging and problem diagnosis for developers;
     * it is not intended to provide any errors suitable for displaying to an
     * end user.
     */
    debug_log?: DebugCallback;
};
/**
 * Parses Markdown.
 */
export type ParseFunction = (source: string, options: ParserOptions) => void;
/**
 * Output callback.
 */
export type OutputCallback = (text: string) => void;
/**
 * HTML converter options.
 */
export type HTMLConverterOptions = {
    /**
     * - Parser {@link Flag }s OR-ed together.
     */
    parser_flags: number;
    /**
     * - Converter {@link HTMLFlag }s OR-ed together.
     */
    renderer_flags: number;
    /**
     * - Output callback.
     */
    process_output: OutputCallback;
};
/**
 * Converts Markdown to HTML.
 */
export type HTMLFunction = (source: string, options: HTMLConverterOptions) => void;
/**
 * Flags specifying extensions/deviations from CommonMark specification.
 * By default (when flags == 0), we follow CommonMark specification.
 * The following flags may allow some extensions or deviations from it.
 * @typedef {object} Flags
 * @property {number} COLLAPSEWHITESPACE - In text.NORMAL, collapse non-trivial whitespace into single ' '
 * @property {number} PERMISSIVEATXHEADERS - Do not require space in ATX headers ( ###header )
 * @property {number} PERMISSIVEURLAUTOLINKS - Recognize URLs as autolinks even without '<', '>'
 * @property {number} PERMISSIVEEMAILAUTOLINKS - Recognize e-mails as autolinks even without '<', '>' and 'mailto:'
 * @property {number} NOINDENTEDCODEBLOCKS - Disable indented code blocks. (Only fenced code works.)
 * @property {number} NOHTMLBLOCKS - Disable raw HTML blocks.
 * @property {number} NOHTMLSPANS - Disable raw HTML (inline).
 * @property {number} TABLES - Enable tables extension.
 * @property {number} STRIKETHROUGH - Enable strikethrough extension.
 * @property {number} PERMISSIVEWWWAUTOLINKS - Enable WWW autolinks (even without any scheme prefix, if they begin with 'www.')
 * @property {number} TASKLISTS - Enable task list extension.
 * @property {number} LATEXMATHSPANS - Enable $ and $$ containing LaTeX equations.
 * @property {number} WIKILINKS - Enable wiki links extension.
 * @property {number} UNDERLINE - Enable underline extension (and disables '_' for normal emphasis).
 * @property {number} PERMISSIVEAUTOLINKS - PERMISSIVEEMAILAUTOLINKS | PERMISSIVEURLAUTOLINKS | PERMISSIVEWWWAUTOLINKS.
 * @property {number} NOHTML - NOHTMLBLOCKS | NOHTMLSPANS.
 * @property {number} DIALECT_COMMONMARK - Commonmark dialect.
 * @property {number} DIALECT_GITHUB - Github dialect.
 */
/**
 * Block type.
 * @typedef {number} BlockType
 */
/**
 * Block represents a part of document hierarchy structure like a paragraph or list item.
 * @typedef {object} BlockTypes
 * @property {BlockType} DOC - BODY.../BODY.
 * @property {BlockType} QUOTE - BLOCKQUOTE.../BLOCKQUOTE.
 * @property {BlockType} UL - UL.../UL Detail: {@link ULDetail}.
 * @property {BlockType} OL - OL.../OL Detail: {@link OLDetail}.
 * @property {BlockType} LI - LI.../LI Detail: {@link LIDetail}.
 * @property {BlockType} HR - HR.
 * @property {BlockType} H - H1.../H1 (for levels up to 6) Detail: {@link HDetail}.
 * @property {BlockType} CODE - PRECODE.../CODE/PRE
 * Note the text lines within code blocks are terminated with '\n' instead of explicit text.BR.
 * Detail: {@link CODEDetail}
 * @property {BlockType} HTML - Raw HTML block. This itself does not correspond to any particular HTML
 * tag. The contents of it _is_ raw HTML source intended to be put
 * in verbatim form to the HTML output.
 * @property {BlockType} P - P.../P.
 * @property {BlockType} TABLE - TABLE.../TABLE Detail: {@link TABLEDetail}.
 * @property {BlockType} THEAD - THEAD.../THEAD.
 * @property {BlockType} TBODY - TBODY.../TBODY.
 * @property {BlockType} TR - TR.../TR.
 * @property {BlockType} TH - TH.../TH Detail: {@link TDDetail}.
 * @property {BlockType} TD - TD.../TD Detail: {@link TDDetail}.
 */
/**
 * Span type.
 * @typedef {number} SpanType
 */
/**
 * Span represents an in-line piece of a document which should be rendered with
 * the same font, color and other attributes. A sequence of spans forms a block
 * like paragraph or list item.
 * @typedef {object} SpanTypes
 * @property {SpanType} EM - EM.../EM.
 * @property {SpanType} STRONG - STRONG.../STRONG.
 * @property {SpanType} A - A HREF="XXX".../A Detail: {@link ADetail}.
 * @property {SpanType} IMG - IMG SRC="XXX".../A
 * Note: Image text can contain nested spans and even nested images.
 * If rendered into ALT attribute of HTML IMG tag, it's responsibility
 * of the parser to deal with it.
 * Detail: {@link IMGDetail}.
 * @property {SpanType} CODE - CODE.../CODE
 * @property {SpanType} DEL - DEL.../DEL
 * Note: Recognized only when flag.STRIKETHROUGH is enabled.
 * @property {SpanType} LATEXMATH - For recognizing inline ($) equations
 * Note: Recognized only when flag.LATEXMATHSPANS is enabled.
 * @property {SpanType} LATEXMATH_DISPLAY - For recognizing display ($$) equations
 * Note: Recognized only when flag.LATEXMATHSPANS is enabled.
 * @property {SpanType} WIKILINK - /* Wiki links
 * Note: Recognized only when flag.WIKILINKS is enabled.
 * Detail: {@link WIKILINKDetail}
 * @property {SpanType} U - U.../U
 * Note: Recognized only when flag.UNDERLINE is enabled.
 */
/**
 * Text type.
 * @typedef {number} TextType
 */
/**
 * Text is the actual textual contents of span.
 * @typedef {object} TextTypes
 * @property {TextType} NORMAL - Normal text.
 * @property {TextType} NULLCHAR - NULL character. CommonMark requires replacing NULL character with
 * the replacement char U+FFFD, so this allows caller to do that easily.
 * @property {TextType} BR - Line breaks.
 * Note these are not sent from blocks with verbatim output (block.CODE
 * or block.HTML). In such cases, '\n' is part of the text itself.
 * BR (hard break).
 * @property {TextType} SOFTBR - '\n' in source text where it is not semantically meaningful (soft break).
 * @property {TextType} ENTITY - Entity.
 * (a) Named entity, e.g. &nbsp;
 * (Note MD4C does not have a list of known entities.
 * Anything matching the regexp /&[A-Za-z][A-Za-z0-9]{1,47};/ is
 * treated as a named entity.)
 * (b) Numerical entity, e.g. &#1234;
 * (c) Hexadecimal entity, e.g. &#x12AB;
 * As MD4C is mostly encoding agnostic, application gets the verbatim
 * entity text into the text_callback().
 * @property {TextType} CODE - Text in a code block (inside block.CODE) or inlined code (`code`).
 * If it is inside block.CODE, it includes spaces for indentation and
 * '\n' for new lines. text.BR and text.SOFTBR are not sent for this
 * kind of text.
 * @property {TextType} HTML - Text is a raw HTML. If it is contents of a raw HTML block (i.e. not
 * an inline raw HTML), then text.BR and text.SOFTBR are not used.
 * The text contains verbatim '\n' for the new lines.
 * @property {TextType} LATEXMATH - Text is inside an equation. This is processed the same way as inlined code
 * spans (`code`).
 */
/**
 * Align type.
 * @typedef {number} AlignType
 */
/**
 * Alignment.
 * @typedef {object} AlignTypes
 * @property {AlignType} DEFAULT - When unspecified.
 * @property {AlignType} LEFT - Align left.
 * @property {AlignType} CENTER - Align center.
 * @property {AlignType} RIGHT - Align right.
 */
/**
 * HTML converter flags.
 * @typedef {object} HTMLFlags
 * @property {number} DEBUG - If set, debug output from parsing is sent to stderr.
 * @property {number} VERBATIM_ENTITIES - Render entities as is.
 * @property {number} XHTML - Follow XTML style e.g. BR / not BR.
 */
/**
 * Constants.
 * @typedef {object} Constants
 * @property {Flags} flag - Parser flags.
 * @property {BlockTypes} block - Block types.
 * @property {SpanTypes} span - Span types.
 * @property {TextTypes} text - Text types.
 * @property {AlignTypes} align - Align types.
 * @property {HTMLFlags} html - HTML flags.
 */
/**
 * Attribute item.
 * @typedef {object} AttributeItem
 * @property {TextType} type - Text type.
 * @property {string} text - Text.
 */
/** Attribute.
 * This wraps strings which are outside of a normal text flow and which are
 * propagated within various detailed structures, but which still may contain
 * string portions of different types like e.g. entities.
 *
 * So, for example, lets consider this image:
 *
 * ![image alt text](http://example.org/image.png 'foo &quot; bar')
 *
 * The image alt text is propagated as a normal text via the text()
 * callback. However, the image title ('foo &quot; bar') is propagated as
 * Attribute in IMGDetail.title.
 *
 * Then the attribute IMGDetail.title shall provide the following:
 *  -- [0]: "foo "   text.NORMAL
 *  -- [1]: "&quot;" text.ENTITY
 *  -- [2]: " bar"   text.NORMAL
 *
 * Note that these invariants are always guaranteed:
 *  -- Currently, only text.NORMAL, text.ENTITY, text.NULLCHAR
 *     substrings can appear. This could change only of the specification
 *     changes.
 * @typedef {AttributeItem[]} Attribute
 */
/**
 * Detailed info for UL block.
 * @typedef {object} ULDetail
 * @property {bool} is_tight - True if tight list, false if loose.
 * @property {string} mark - Item bullet character in Markdown source of the list, e.g. '-', '+', '*'.
 */
/**
 * Detailed info for OL block.
 * @typedef {object} OLDetail
 * @property {number} start - Start index of the ordered list.
 * @property {bool} is_tight - True if tight list, false if loose.
 * @property {string} mark_delimiter - Character delimiting the item marks in MarkDown source, e.g. '.' or ')'
 */
/**
 * Detailed info for UL block.
 * @typedef {object} LIDetail
 * @property {bool} is_task - Can be non-zero only with flag.TASKLISTS.
 * @property {string|undefined} task_mark - If is_task, then one of 'x', 'X' or ' '. Undefined otherwise.
 * @property {number} task_mark_offset - If is_task, then offset in the input of the char between '[' and ']'.
 */
/**
 * Detailed info for H block.
 * @typedef {object} HDetail
 * @property {number} level - Header level (1 - 6).
 */
/**
 * Detailed info for CODE block.
 * @typedef {object} CODEDetail
 * @property {Attribute} info - Code info.
 * @property {Attribute} lang - Code language.
 * @property {string} fence_char - The character used for fenced code block; or zero for indented code block.
 */
/**
 * Detailed info for TABLE block.
 * @typedef {object} TABLEDetail
 * @property {number} col_count - Count of columns in the table.
 * @property {number} head_row_count - Count of rows in the table header (currently always 1).
 * @property {number} body_row_count - Count of rows in the table body.
 */
/**
 * Detailed info for TD and TH block.
 * @typedef {object} TDDetail
 * @property {AlignType} align - Table cell align.
 */
/**
 * Detailed info for block.
 * @typedef {null|ULDetail|OLDetail|LIDetail|HDetail|CODEDetail|TABLEDetail|TDDetail} BlockDetail
 */
/**
 * Detailed info for A span.
 * @typedef {object} ADetail
 * @property {Attribute} href - URL.
 * @property {Attribute} title - Title.
 */
/**
 * Detailed info for IMG span.
 * @typedef {object} IMGDetail
 * @property {Attribute} src - Image source.
 * @property {Attribute} title - Image title.
 */
/**
 * Detailed info for WIKILINK span.
 * @typedef {object} WIKILINKDetail
 * @property {Attribute} target - Wikilink target.
 */
/**
 * Detailed info for span.
 * @typedef {null|ADetail|IMGDetail|WIKILINKDetail} SpanDetail
 */
/**
 * Block callback.
 * @callback BlockCallback
 * @param {BlockType} type - Block type.
 * @param {BlockDetail} detail - Detail.
 * @returns {void}
 */
/**
 * Span callback.
 * @callback SpanCallback
 * @param {SpanType} type - Span type.
 * @param {SpanDetail} detail - Detail.
 * @returns {void}
 */
/**
 * Text callback.
 * @callback TextCallback
 * @param {TextType} type - Text type.
 * @param {string} text - Text.
 * @returns {void}
 */
/**
 * Debug callback.
 * @callback DebugCallback
 * @param {string} message - Message.
 * @returns {void}
 */
/**
 * Markdown parser options.
 * @typedef {object} ParserOptions
 * @property {number} flags - Parser {@link Flag}s OR-ed together.
 * @property {BlockCallback} enter_block - Enter block callback.
 * @property {BlockCallback} leave_block - Leave block callback.
 * @property {SpanCallback} enter_span - Enter span callback.
 * @property {SpanCallback} leave_span - Leave span callback.
 * @property {TextCallback} text - Text callback.
 * @property {DebugCallback} [debug_log] - Debug callback. Optional.
 * If provided and something goes wrong, this function gets called.
 * This is intended for debugging and problem diagnosis for developers;
 * it is not intended to provide any errors suitable for displaying to an
 * end user.
 */
/**
 * Parses Markdown.
 * @callback ParseFunction
 * @param {string} source - Markdown source.
 * @param {ParserOptions} options - Parser options.
 * @returns {void}
 */
/**
 * Output callback.
 * @callback OutputCallback
 * @param {string} text - Message.
 * @returns {void}
 */
/**
 * HTML converter options.
 * @typedef {object} HTMLConverterOptions
 * @property {number} parser_flags - Parser {@link Flag}s OR-ed together.
 * @property {number} renderer_flags - Converter {@link HTMLFlag}s OR-ed together.
 * @property {OutputCallback} process_output - Output callback.
 */
/**
 * Converts Markdown to HTML.
 * @callback HTMLFunction
 * @param {string} source - Markdown source.
 * @param {HTMLConverterOptions} options - Converter options.
 * @returns {void}
 */
/**
 * Constants.
 * @type {Constants}
 */
export const constants: Constants;
/**
 * Parse the Markdown document stored in the string source.
 * The parser provides callbacks to be called during the parsing so the
 * caller can render the document on the screen or convert the Markdown
 * to another format.
 * @type {ParseFunction}
 */
export const parse: ParseFunction;
/**
 * Render Markdown into HTML.
 * Note only contents of BODY tag is generated. Caller must generate
 * HTML header/footer manually before/after calling html().
 * @type {HTMLFunction}
 */
export const html: HTMLFunction;
