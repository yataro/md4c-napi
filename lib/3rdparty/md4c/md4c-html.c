/*
 * MD4C: Markdown parser for C
 * (http://github.com/mity/md4c)
 *
 * Copyright (c) 2016-2019 Martin Mitas
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/*
 * This file contains modifications related to md4c-napi project.
 * All these modifications are marked with a comment with "md4c-napi" mark.
 * All these modifications are licensed under this license:
 *
 * Copyright (c) 2023 Aikawa Yataro
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include <string.h>

#include "md4c-html.h"
#include "entity.h"


#if !defined(__STDC_VERSION__) || __STDC_VERSION__ < 199409L
    /* C89/90 or old compilers in general may not understand "inline". */
    #if defined __GNUC__
        #define inline __inline__
    #elif defined _MSC_VER
        #define inline __inline
    #else
        #define inline
    #endif
#endif

/* md4c-napi: UTF-16 support for HTML converter */
/* Make the UTF-8 support the default. */
#if !defined MD4C_USE_ASCII && !defined MD4C_USE_UTF8 && !defined MD4C_USE_UTF16
    #define MD4C_USE_UTF8
#endif

/* Magic for making wide literals with MD4C_USE_UTF16. */
#ifdef _T
    #undef _T
#endif

#if defined MD4C_USE_UTF16
    #define _T(x)           u##x
#else
    #define _T(x)           x
#endif



typedef struct MD_HTML_tag MD_HTML;
struct MD_HTML_tag {
    void (*process_output)(const MD_CHAR*, MD_SIZE, void*);
    void* userdata;
    unsigned flags;
    int image_nesting_level;
    char escape_map[256];
};

#define NEED_HTML_ESC_FLAG   0x1
#define NEED_URL_ESC_FLAG    0x2


/*****************************************
 ***  HTML rendering helper functions  ***
 *****************************************/

/* md4c-napi: UTF-16 support for HTML converter */
#define ISDIGIT(ch)     (_T('0') <= (ch) && (ch) <= _T('9'))
#define ISLOWER(ch)     (_T('a') <= (ch) && (ch) <= _T('z'))
#define ISUPPER(ch)     (_T('A') <= (ch) && (ch) <= _T('Z'))
#define ISALNUM(ch)     (ISLOWER(ch) || ISUPPER(ch) || ISDIGIT(ch))
#define ISMBYTE(ch)     ((ch & 0xffffff00) != 0)

/* md4c-napi: UTF-16 support for HTML converter */
#if defined MD4C_USE_UTF16
    static size_t _md_strlen(const MD_CHAR *str) {
        const MD_CHAR *begin = str;
        while(*str) str++;

        return str - begin;
    }

    #define md_strlen _md_strlen
#else
    #define md_strlen strlen
#endif

static inline void
render_verbatim(MD_HTML* r, const MD_CHAR* text, MD_SIZE size)
{
    r->process_output(text, size, r->userdata);
}

/* md4c-napi: UTF-16 support for HTML converter */
/* Keep this as a macro. Most compiler should then be smart enough to replace
 * the strlen() call with a compile-time constant if the string is a C literal. */
#define RENDER_VERBATIM(r, verbatim)                                    \
        render_verbatim((r), (verbatim), (MD_SIZE) (md_strlen(verbatim)))


static void
render_html_escaped(MD_HTML* r, const MD_CHAR* data, MD_SIZE size)
{
    MD_OFFSET beg = 0;
    MD_OFFSET off = 0;

    /* Some characters need to be escaped in normal HTML text. */
#if defined MD4C_USE_UTF16
    #define NEED_HTML_ESC(ch)   (!ISMBYTE(ch) && r->escape_map[(unsigned char)(ch)] & NEED_HTML_ESC_FLAG)
#else 
    #define NEED_HTML_ESC(ch)   (r->escape_map[(unsigned char)(ch)] & NEED_HTML_ESC_FLAG)
#endif

    while(1) {
        /* Optimization: Use some loop unrolling. */
        while(off + 3 < size  &&  !NEED_HTML_ESC(data[off+0])  &&  !NEED_HTML_ESC(data[off+1])
                              &&  !NEED_HTML_ESC(data[off+2])  &&  !NEED_HTML_ESC(data[off+3]))
            off += 4;
        while(off < size  &&  !NEED_HTML_ESC(data[off]))
            off++;

        if(off > beg)
            render_verbatim(r, data + beg, off - beg);

        if(off < size) {
            switch(data[off]) {
                case _T('&'):   RENDER_VERBATIM(r, _T("&amp;")); break;
                case _T('<'):   RENDER_VERBATIM(r, _T("&lt;")); break;
                case _T('>'):   RENDER_VERBATIM(r, _T("&gt;")); break;
                case _T('"'):   RENDER_VERBATIM(r, _T("&quot;")); break;
            }
            off++;
        } else {
            break;
        }
        beg = off;
    }
}

/* md4c-napi: UTF-16 support for HTML converter */
static void
render_url_escaped(MD_HTML* r, const MD_CHAR* data, MD_SIZE size)
{
    static const MD_CHAR hex_chars[] = _T("0123456789ABCDEF");
    MD_OFFSET beg = 0;
    MD_OFFSET off = 0;

    /* Some characters need to be escaped in URL attributes. */
#if defined MD4C_USE_UTF16
    #define NEED_URL_ESC(ch)   (!ISMBYTE(ch) && r->escape_map[(unsigned char)(ch)] & NEED_URL_ESC_FLAG)
#else 
    #define NEED_URL_ESC(ch)    (r->escape_map[(unsigned char)(ch)] & NEED_URL_ESC_FLAG)
#endif

    while(1) {
        while(off < size  &&  !NEED_URL_ESC(data[off]))
            off++;
        if(off > beg)
            render_verbatim(r, data + beg, off - beg);

        if(off < size) {
            MD_CHAR hex[3];

            switch(data[off]) {
                case _T('&'):   RENDER_VERBATIM(r, _T("&amp;")); break;
                default:
                    /* This is completely safe because all chars with NEED_URL_ESC_FLAG are utf8<=>utf16 interconvertible */
                    hex[0] = _T('%');
                    hex[1] = hex_chars[((unsigned)data[off] >> 4) & 0xf];
                    hex[2] = hex_chars[((unsigned)data[off] >> 0) & 0xf];
                    render_verbatim(r, hex, 3);
                    break;
            }
            off++;
        } else {
            break;
        }

        beg = off;
    }
}

/* md4c-napi: UTF-16 support for HTML converter */
static unsigned
hex_val(MD_CHAR ch)
{
    if(_T('0') <= ch && ch <= _T('9'))
        return ch - _T('0');
    if(_T('A') <= ch && ch <= _T('Z'))
        return ch - _T('A') + 10;
    else
        return ch - _T('a') + 10;
}

/* md4c-napi: UTF-16 support for HTML converter */
static void
render_codepoint(MD_HTML* r, unsigned codepoint,
                      void (*fn_append)(MD_HTML*, const MD_CHAR*, MD_SIZE))
{
#ifdef MD4C_USE_UTF16
    static const MD_CHAR replacement_char[] = { 0xfffd };
    MD_CHAR chars[2];
#else
    static const MD_CHAR replacement_char[] = { 0xef, 0xbf, 0xbd };
    unsigned char chars[4];
#endif

    size_t n = 0;

#ifdef MD4C_USE_UTF16
    if (codepoint <= 0xd7ff) {
        n = 1;
        chars[0] = codepoint;
    } else if (codepoint <= 0xdfff) {
        n = 0;
    } else if (codepoint <= 0x7ff) {
        n = 1;
        chars[0] = codepoint;
    } else if (codepoint <= 0x10ffff) {
        n = 2;
        codepoint -= 0x10000;
        chars[0] = 0xd800 | ((codepoint >> 10) & 0x3ff0);
        chars[1] = 0xdc00 | ((codepoint >>  0) & 0x3ff0);
    }
#else
    if(codepoint <= 0x7f) {
        n = 1;
        chars[0] = codepoint;
    } else if(codepoint <= 0x7ff) {
        n = 2;
        chars[0] = 0xc0 | ((codepoint >>  6) & 0x1f);
        chars[1] = 0x80 + ((codepoint >>  0) & 0x3f);
    } else if(codepoint <= 0xffff) {
        n = 3;
        chars[0] = 0xe0 | ((codepoint >> 12) & 0xf);
        chars[1] = 0x80 + ((codepoint >>  6) & 0x3f);
        chars[2] = 0x80 + ((codepoint >>  0) & 0x3f);
    } else if (codepoint <= 0x10ffff) {
        n = 4;
        chars[0] = 0xf0 | ((codepoint >> 18) & 0x7);
        chars[1] = 0x80 + ((codepoint >> 12) & 0x3f);
        chars[2] = 0x80 + ((codepoint >>  6) & 0x3f);
        chars[3] = 0x80 + ((codepoint >>  0) & 0x3f);
    }
#endif

    if(n)
        fn_append(r, (MD_CHAR *)chars, (MD_SIZE)n);
    else
        fn_append(r, replacement_char, sizeof(replacement_char) / sizeof(replacement_char[0]));
}

/* md4c-napi: UTF-16 support for HTML converter */
/* Translate entity to its decoded equivalent, or output the verbatim one
 * if such entity is unknown (or if the translation is disabled). */
static void
render_entity(MD_HTML* r, const MD_CHAR* text, MD_SIZE size,
              void (*fn_append)(MD_HTML*, const MD_CHAR*, MD_SIZE))
{
    if(r->flags & MD_HTML_FLAG_VERBATIM_ENTITIES) {
        render_verbatim(r, text, size);
        return;
    }

    if(size > 3 && text[1] == _T('#')) {
        unsigned codepoint = 0;

        if(text[2] == _T('x') || text[2] == _T('X')) {
            /* Hexadecimal entity (e.g. "&#x1234abcd;")). */
            MD_SIZE i;
            for(i = 3; i < size-1; i++)
                codepoint = 16 * codepoint + hex_val(text[i]);
        } else {
            /* Decimal entity (e.g. "&1234;") */
            MD_SIZE i;
            for(i = 2; i < size-1; i++)
                codepoint = 10 * codepoint + (text[i] - _T('0'));
        }

        render_codepoint(r, codepoint, fn_append);
        return;
    } else {
        /* Named entity (e.g. "&nbsp;"). */
        const struct entity* ent;

        ent = entity_lookup(text, size);
        if(ent != NULL) {
            render_codepoint(r, ent->codepoints[0], fn_append);
            if(ent->codepoints[1])
                render_codepoint(r, ent->codepoints[1], fn_append);
            return;
        }
    }

    fn_append(r, text, size);
}

static void
render_attribute(MD_HTML* r, const MD_ATTRIBUTE* attr,
                 void (*fn_append)(MD_HTML*, const MD_CHAR*, MD_SIZE))
{
    int i;

    for(i = 0; attr->substr_offsets[i] < attr->size; i++) {
        MD_TEXTTYPE type = attr->substr_types[i];
        MD_OFFSET off = attr->substr_offsets[i];
        MD_SIZE size = attr->substr_offsets[i+1] - off;
        const MD_CHAR* text = attr->text + off;

        switch(type) {
            case MD_TEXT_NULLCHAR:  render_codepoint(r, 0x0000, render_verbatim); break;
            case MD_TEXT_ENTITY:    render_entity(r, text, size, fn_append); break;
            default:                fn_append(r, text, size); break;
        }
    }
}

/* md4c-napi: UTF-16 support for HTML converter */
static MD_CHAR *md_itoa(MD_CHAR *p, unsigned x) {
    p += sizeof(int) * 3;
    *--p = 0;
    do {
        *--p = '0' + x % 10;
        x /= 10;
    } while (x);

    return p;
}

/* md4c-napi: UTF-16 support for HTML converter */
static void
render_open_ol_block(MD_HTML* r, const MD_BLOCK_OL_DETAIL* det)
{
    MD_CHAR buf[sizeof(int) * 3];

    if(det->start == 1) {
        RENDER_VERBATIM(r, _T("<ol>\n"));
        return;
    }

    RENDER_VERBATIM(r, _T("<ol start=\""));
    RENDER_VERBATIM(r, md_itoa(buf, det->start));
    RENDER_VERBATIM(r, _T("\">\n"));
}

/* md4c-napi: UTF-16 support for HTML converter */
static void
render_open_li_block(MD_HTML* r, const MD_BLOCK_LI_DETAIL* det)
{
    if(det->is_task) {
        RENDER_VERBATIM(r, _T("<li class=\"task-list-item\">"
                          "<input type=\"checkbox\" class=\"task-list-item-checkbox\" disabled"));
        if(det->task_mark == _T('x') || det->task_mark == _T('X'))
            RENDER_VERBATIM(r, _T(" checked"));
        RENDER_VERBATIM(r, _T(">"));
    } else {
        RENDER_VERBATIM(r, _T("<li>"));
    }
}

/* md4c-napi: UTF-16 support for HTML converter */
static void
render_open_code_block(MD_HTML* r, const MD_BLOCK_CODE_DETAIL* det)
{
    RENDER_VERBATIM(r, _T("<pre><code"));

    /* If known, output the HTML 5 attribute class="language-LANGNAME". */
    if(det->lang.text != NULL) {
        RENDER_VERBATIM(r, _T(" class=\"language-"));
        render_attribute(r, &det->lang, render_html_escaped);
        RENDER_VERBATIM(r, _T("\""));
    }

    RENDER_VERBATIM(r, _T(">"));
}

/* md4c-napi: UTF-16 support for HTML converter */
static void
render_open_td_block(MD_HTML* r, const MD_CHAR* cell_type, const MD_BLOCK_TD_DETAIL* det)
{
    RENDER_VERBATIM(r, _T("<"));
    RENDER_VERBATIM(r, cell_type);

    switch(det->align) {
        case MD_ALIGN_LEFT:     RENDER_VERBATIM(r, _T(" align=\"left\">")); break;
        case MD_ALIGN_CENTER:   RENDER_VERBATIM(r, _T(" align=\"center\">")); break;
        case MD_ALIGN_RIGHT:    RENDER_VERBATIM(r, _T(" align=\"right\">")); break;
        default:                RENDER_VERBATIM(r, _T(">")); break;
    }
}

/* md4c-napi: UTF-16 support for HTML converter */
static void
render_open_a_span(MD_HTML* r, const MD_SPAN_A_DETAIL* det)
{
    RENDER_VERBATIM(r, _T("<a href=\""));
    render_attribute(r, &det->href, render_url_escaped);

    if(det->title.text != NULL) {
        RENDER_VERBATIM(r, _T("\" title=\""));
        render_attribute(r, &det->title, render_html_escaped);
    }

    RENDER_VERBATIM(r, _T("\">"));
}

/* md4c-napi: UTF-16 support for HTML converter */
static void
render_open_img_span(MD_HTML* r, const MD_SPAN_IMG_DETAIL* det)
{
    RENDER_VERBATIM(r, _T("<img src=\""));
    render_attribute(r, &det->src, render_url_escaped);

    RENDER_VERBATIM(r, _T("\" alt=\""));

    r->image_nesting_level++;
}

/* md4c-napi: UTF-16 support for HTML converter */
static void
render_close_img_span(MD_HTML* r, const MD_SPAN_IMG_DETAIL* det)
{
    if(det->title.text != NULL) {
        RENDER_VERBATIM(r, _T("\" title=\""));
        render_attribute(r, &det->title, render_html_escaped);
    }

    RENDER_VERBATIM(r, (r->flags & MD_HTML_FLAG_XHTML) ? _T("\" />") : _T("\">"));

    r->image_nesting_level--;
}

/* md4c-napi: UTF-16 support for HTML converter */
static void
render_open_wikilink_span(MD_HTML* r, const MD_SPAN_WIKILINK_DETAIL* det)
{
    RENDER_VERBATIM(r, _T("<x-wikilink data-target=\""));
    render_attribute(r, &det->target, render_html_escaped);

    RENDER_VERBATIM(r, _T("\">"));
}


/**************************************
 ***  HTML renderer implementation  ***
 **************************************/

/* md4c-napi: UTF-16 support for HTML converter */
static int
enter_block_callback(MD_BLOCKTYPE type, void* detail, void* userdata)
{
    static const MD_CHAR* head[6] = { _T("<h1>"), _T("<h2>"), _T("<h3>"), _T("<h4>"), _T("<h5>"), _T("<h6>") };
    MD_HTML* r = (MD_HTML*) userdata;

    switch(type) {
        case MD_BLOCK_DOC:      /* noop */ break;
        case MD_BLOCK_QUOTE:    RENDER_VERBATIM(r, _T("<blockquote>\n")); break;
        case MD_BLOCK_UL:       RENDER_VERBATIM(r, _T("<ul>\n")); break;
        case MD_BLOCK_OL:       render_open_ol_block(r, (const MD_BLOCK_OL_DETAIL*)detail); break;
        case MD_BLOCK_LI:       render_open_li_block(r, (const MD_BLOCK_LI_DETAIL*)detail); break;
        case MD_BLOCK_HR:       RENDER_VERBATIM(r, (r->flags & MD_HTML_FLAG_XHTML) ? _T("<hr />\n") : _T("<hr>\n")); break;
        case MD_BLOCK_H:        RENDER_VERBATIM(r, head[((MD_BLOCK_H_DETAIL*)detail)->level - 1]); break;
        case MD_BLOCK_CODE:     render_open_code_block(r, (const MD_BLOCK_CODE_DETAIL*) detail); break;
        case MD_BLOCK_HTML:     /* noop */ break;
        case MD_BLOCK_P:        RENDER_VERBATIM(r, _T("<p>")); break;
        case MD_BLOCK_TABLE:    RENDER_VERBATIM(r, _T("<table>\n")); break;
        case MD_BLOCK_THEAD:    RENDER_VERBATIM(r, _T("<thead>\n")); break;
        case MD_BLOCK_TBODY:    RENDER_VERBATIM(r, _T("<tbody>\n")); break;
        case MD_BLOCK_TR:       RENDER_VERBATIM(r, _T("<tr>\n")); break;
        case MD_BLOCK_TH:       render_open_td_block(r, _T("th"), (MD_BLOCK_TD_DETAIL*)detail); break;
        case MD_BLOCK_TD:       render_open_td_block(r, _T("td"), (MD_BLOCK_TD_DETAIL*)detail); break;
    }

    return 0;
}

/* md4c-napi: UTF-16 support for HTML converter */
static int
leave_block_callback(MD_BLOCKTYPE type, void* detail, void* userdata)
{
    static const MD_CHAR* head[6] = { _T("</h1>\n"), _T("</h2>\n"), _T("</h3>\n"), _T("</h4>\n"), _T("</h5>\n"), _T("</h6>\n") };
    MD_HTML* r = (MD_HTML*) userdata;

    switch(type) {
        case MD_BLOCK_DOC:      /*noop*/ break;
        case MD_BLOCK_QUOTE:    RENDER_VERBATIM(r, _T("</blockquote>\n")); break;
        case MD_BLOCK_UL:       RENDER_VERBATIM(r, _T("</ul>\n")); break;
        case MD_BLOCK_OL:       RENDER_VERBATIM(r, _T("</ol>\n")); break;
        case MD_BLOCK_LI:       RENDER_VERBATIM(r, _T("</li>\n")); break;
        case MD_BLOCK_HR:       /*noop*/ break;
        case MD_BLOCK_H:        RENDER_VERBATIM(r, head[((MD_BLOCK_H_DETAIL*)detail)->level - 1]); break;
        case MD_BLOCK_CODE:     RENDER_VERBATIM(r, _T("</code></pre>\n")); break;
        case MD_BLOCK_HTML:     /* noop */ break;
        case MD_BLOCK_P:        RENDER_VERBATIM(r, _T("</p>\n")); break;
        case MD_BLOCK_TABLE:    RENDER_VERBATIM(r, _T("</table>\n")); break;
        case MD_BLOCK_THEAD:    RENDER_VERBATIM(r, _T("</thead>\n")); break;
        case MD_BLOCK_TBODY:    RENDER_VERBATIM(r, _T("</tbody>\n")); break;
        case MD_BLOCK_TR:       RENDER_VERBATIM(r, _T("</tr>\n")); break;
        case MD_BLOCK_TH:       RENDER_VERBATIM(r, _T("</th>\n")); break;
        case MD_BLOCK_TD:       RENDER_VERBATIM(r, _T("</td>\n")); break;
    }

    return 0;
}

/* md4c-napi: UTF-16 support for HTML converter */
static int
enter_span_callback(MD_SPANTYPE type, void* detail, void* userdata)
{
    MD_HTML* r = (MD_HTML*) userdata;

    if(r->image_nesting_level > 0) {
        /* We are inside a Markdown image label. Markdown allows to use any
         * emphasis and other rich contents in that context similarly as in
         * any link label.
         *
         * However, unlike in the case of links (where that contents becomes
         * contents of the <a>...</a> tag), in the case of images the contents
         * is supposed to fall into the attribute alt: <img alt="...">.
         *
         * In that context we naturally cannot output nested HTML tags. So lets
         * suppress them and only output the plain text (i.e. what falls into
         * text() callback).
         *
         * This make-it-a-plain-text approach is the recommended practice by
         * CommonMark specification (for HTML output).
         */
        return 0;
    }

    switch(type) {
        case MD_SPAN_EM:                RENDER_VERBATIM(r, _T("<em>")); break;
        case MD_SPAN_STRONG:            RENDER_VERBATIM(r, _T("<strong>")); break;
        case MD_SPAN_U:                 RENDER_VERBATIM(r, _T("<u>")); break;
        case MD_SPAN_A:                 render_open_a_span(r, (MD_SPAN_A_DETAIL*) detail); break;
        case MD_SPAN_IMG:               render_open_img_span(r, (MD_SPAN_IMG_DETAIL*) detail); break;
        case MD_SPAN_CODE:              RENDER_VERBATIM(r, _T("<code>")); break;
        case MD_SPAN_DEL:               RENDER_VERBATIM(r, _T("<del>")); break;
        case MD_SPAN_LATEXMATH:         RENDER_VERBATIM(r, _T("<x-equation>")); break;
        case MD_SPAN_LATEXMATH_DISPLAY: RENDER_VERBATIM(r, _T("<x-equation type=\"display\">")); break;
        case MD_SPAN_WIKILINK:          render_open_wikilink_span(r, (MD_SPAN_WIKILINK_DETAIL*) detail); break;
    }

    return 0;
}

/* md4c-napi: UTF-16 support for HTML converter */
static int
leave_span_callback(MD_SPANTYPE type, void* detail, void* userdata)
{
    MD_HTML* r = (MD_HTML*) userdata;

    if(r->image_nesting_level > 0) {
        /* Ditto as in enter_span_callback(), except we have to allow the
         * end of the <img> tag. */
        if(r->image_nesting_level == 1  &&  type == MD_SPAN_IMG)
            render_close_img_span(r, (MD_SPAN_IMG_DETAIL*) detail);
        return 0;
    }

    switch(type) {
        case MD_SPAN_EM:                RENDER_VERBATIM(r, _T("</em>")); break;
        case MD_SPAN_STRONG:            RENDER_VERBATIM(r, _T("</strong>")); break;
        case MD_SPAN_U:                 RENDER_VERBATIM(r, _T("</u>")); break;
        case MD_SPAN_A:                 RENDER_VERBATIM(r, _T("</a>")); break;
        case MD_SPAN_IMG:               /*noop, handled above*/ break;
        case MD_SPAN_CODE:              RENDER_VERBATIM(r, _T("</code>")); break;
        case MD_SPAN_DEL:               RENDER_VERBATIM(r, _T("</del>")); break;
        case MD_SPAN_LATEXMATH:         /*fall through*/
        case MD_SPAN_LATEXMATH_DISPLAY: RENDER_VERBATIM(r, _T("</x-equation>")); break;
        case MD_SPAN_WIKILINK:          RENDER_VERBATIM(r, _T("</x-wikilink>")); break;
    }

    return 0;
}

/* md4c-napi: UTF-16 support for HTML converter */
static int
text_callback(MD_TEXTTYPE type, const MD_CHAR* text, MD_SIZE size, void* userdata)
{
    MD_HTML* r = (MD_HTML*) userdata;

    switch(type) {
        case MD_TEXT_NULLCHAR:  render_codepoint(r, 0x0000, render_verbatim); break;
        case MD_TEXT_BR:        RENDER_VERBATIM(r, (r->image_nesting_level == 0
                                        ? ((r->flags & MD_HTML_FLAG_XHTML) ? _T("<br />\n") : _T("<br>\n"))
                                        : _T(" ")));
                                break;
        case MD_TEXT_SOFTBR:    RENDER_VERBATIM(r, (r->image_nesting_level == 0 ? _T("\n") : _T(" "))); break;
        case MD_TEXT_HTML:      render_verbatim(r, text, size); break;
        case MD_TEXT_ENTITY:    render_entity(r, text, size, render_html_escaped); break;
        default:                render_html_escaped(r, text, size); break;
    }

    return 0;
}

static void
debug_log_callback(const char* msg, void* userdata)
{
    MD_HTML* r = (MD_HTML*) userdata;
    if(r->flags & MD_HTML_FLAG_DEBUG)
        fprintf(stderr, "MD4C: %s\n", msg);
}

/* md4c-napi: UTF-16 support for HTML converter */
int
md_html(const MD_CHAR* input, MD_SIZE input_size,
        void (*process_output)(const MD_CHAR*, MD_SIZE, void*),
        void* userdata, unsigned parser_flags, unsigned renderer_flags)
{
    MD_HTML render = { process_output, userdata, renderer_flags, 0, { 0 } };
    int i;

    MD_PARSER parser = {
        0,
        parser_flags,
        enter_block_callback,
        leave_block_callback,
        enter_span_callback,
        leave_span_callback,
        text_callback,
        debug_log_callback,
        NULL
    };

    /* Build map of characters which need escaping. */
    for(i = 0; i < 256; i++) {
        unsigned char ch = (unsigned char) i;

        if(strchr("\"&<>", ch) != NULL)
            render.escape_map[i] |= NEED_HTML_ESC_FLAG;

        if(!ISALNUM(ch)  &&  strchr("~-_.+!*(),%#@?=;:/,+$", ch) == NULL)
            render.escape_map[i] |= NEED_URL_ESC_FLAG;
    }

/* md4c-napi: Not needed in UTF-16 */
#ifndef MD4C_USE_UTF16
    /* Consider skipping UTF-8 byte order mark (BOM). */
    if(renderer_flags & MD_HTML_FLAG_SKIP_UTF8_BOM  &&  sizeof(MD_CHAR) == 1) {
        static const MD_CHAR bom[3] = { 0xef, 0xbb, 0xbf };
        if(input_size >= sizeof(bom)  &&  memcmp(input, bom, sizeof(bom)) == 0) {
            input += sizeof(bom);
            input_size -= sizeof(bom);
        }
    }
#endif

    return md_parse(input, input_size, &parser, (void*) &render);
}

