#include "parse.hpp"
#include "utils.hpp"

#include <md4c.h>



// X Macro is OK.
#define CALLBACKS			\
	X(enter_block)			\
	X(leave_block)			\
	X(enter_span)			\
	X(leave_span)			\
	X(text)					\
	X_DEBUG_LOG(debug_log)



namespace MD4CNapi {

#define X(callback) Napi::Function callback;
#define X_DEBUG_LOG(callback) X(callback)

struct ParserData {
	Napi::Env env;

CALLBACKS

	inline ParserData() : env(nullptr) {}

	inline int ret() const {
		return env.IsExceptionPending() ? 1 : 0;
	}
};

#undef X
#undef X_DEBUG_LOG

static Napi::Array attribute_array(Napi::Env env, const MD_ATTRIBUTE *attr) {
	auto attr_arr = Napi::Array::New(env);

	for(int i = 0; attr->substr_offsets[i] < attr->size; i++) {
		auto attr_obj = Napi::Object::New(env);

		MD_TEXTTYPE type = attr->substr_types[i];
		MD_OFFSET offset = attr->substr_offsets[i];
		auto *text = attr->text + offset;
		MD_SIZE text_size = attr->substr_offsets[i + 1] - offset;

		attr_obj.Set("type", Napi::Number::New(env, type));
		attr_obj.Set("text", Napi::String::New(env, text, text_size));

		attr_arr[i] = attr_obj;
	}

	return attr_arr;
}

static Napi::Object block_ul_detail(Napi::Env env, const MD_BLOCK_UL_DETAIL *detail) {
	auto detail_obj = Napi::Object::New(env);

	detail_obj.Set("is_tight", Napi::Boolean::New(env, detail->is_tight));
	detail_obj.Set("mark", Napi::String::New(env, &detail->mark, 1));

	return detail_obj;
}

static Napi::Object block_ol_detail(Napi::Env env, const MD_BLOCK_OL_DETAIL *detail) {
	auto detail_obj = Napi::Object::New(env);

	detail_obj.Set("start", Napi::Number::New(env, detail->start));
	detail_obj.Set("is_tight", Napi::Boolean::New(env, detail->is_tight));
	detail_obj.Set("mark_delimiter", Napi::String::New(env, &detail->mark_delimiter, 1));

	return detail_obj;
}

static Napi::Object block_li_detail(Napi::Env env, const MD_BLOCK_LI_DETAIL *detail) {
	auto detail_obj = Napi::Object::New(env);

	detail_obj.Set("is_task", Napi::Boolean::New(env, detail->is_task));
	detail_obj.Set("task_mark", (detail->task_mark == u'\0') ? env.Undefined() : Napi::String::New(env, &detail->task_mark, 1));
	detail_obj.Set("task_mark_offset", Napi::Number::New(env, detail->task_mark_offset));

	return detail_obj;
}

static Napi::Object block_h_detail(Napi::Env env, const MD_BLOCK_H_DETAIL *detail) {
	auto detail_obj = Napi::Object::New(env);

	detail_obj.Set("level", Napi::Number::New(env, detail->level));

	return detail_obj;
}

static Napi::Object block_code_detail(Napi::Env env, const MD_BLOCK_CODE_DETAIL *detail) {
	auto detail_obj = Napi::Object::New(env);

	detail_obj.Set("info", attribute_array(env, &detail->info));
	detail_obj.Set("lang", attribute_array(env, &detail->lang));
	detail_obj.Set("fence_char", Napi::String::New(env, &detail->fence_char, 1));

	return detail_obj;
}

static Napi::Object block_table_detail(Napi::Env env, const MD_BLOCK_TABLE_DETAIL *detail) {
	auto detail_obj = Napi::Object::New(env);

	detail_obj.Set("col_count", Napi::Number::New(env, detail->col_count));
	detail_obj.Set("head_row_count", Napi::Number::New(env, detail->head_row_count));
	detail_obj.Set("body_row_count", Napi::Number::New(env, detail->body_row_count));

	return detail_obj;
}

static Napi::Object block_td_detail(Napi::Env env, const MD_BLOCK_TD_DETAIL *detail) {
	auto detail_obj = Napi::Object::New(env);

	detail_obj.Set("align", Napi::Number::New(env, detail->align));

	return detail_obj;
}

static Napi::Object span_a_detail(Napi::Env env, const MD_SPAN_A_DETAIL *detail) {
	auto detail_obj = Napi::Object::New(env);

	detail_obj.Set("href", attribute_array(env, &detail->href));
	detail_obj.Set("title", attribute_array(env, &detail->title));

	return detail_obj;
}

static Napi::Object span_img_detail(Napi::Env env, const MD_SPAN_IMG_DETAIL *detail) {
	auto detail_obj = Napi::Object::New(env);

	detail_obj.Set("src", attribute_array(env, &detail->src));
	detail_obj.Set("title", attribute_array(env, &detail->title));

	return detail_obj;
}

static Napi::Object span_wikilink_detail(Napi::Env env, const MD_SPAN_WIKILINK_DETAIL *detail) {
	auto detail_obj = Napi::Object::New(env);

	detail_obj.Set("target", attribute_array(env, &detail->target));

	return detail_obj;
}

static Napi::Value block_detail(Napi::Env env, MD_BLOCKTYPE type, void *detail) {
	if (detail == nullptr) {
		return env.Null();
	}

	switch (type) {
		case MD_BLOCK_UL:
			return block_ul_detail(env, static_cast<MD_BLOCK_UL_DETAIL *>(detail));
		case MD_BLOCK_OL:
			return block_ol_detail(env, static_cast<MD_BLOCK_OL_DETAIL *>(detail));
		case MD_BLOCK_LI:
			return block_li_detail(env, static_cast<MD_BLOCK_LI_DETAIL *>(detail));
		case MD_BLOCK_H:
			return block_h_detail(env, static_cast<MD_BLOCK_H_DETAIL *>(detail));
		case MD_BLOCK_CODE:
			return block_code_detail(env, static_cast<MD_BLOCK_CODE_DETAIL *>(detail));
		case MD_BLOCK_TABLE:
			return block_table_detail(env, static_cast<MD_BLOCK_TABLE_DETAIL *>(detail));
		case MD_BLOCK_TH:
		case MD_BLOCK_TD:
			return block_td_detail(env, static_cast<MD_BLOCK_TD_DETAIL *>(detail));

		default:
			break;
	}

	return env.Null();
}

static Napi::Value span_detail(Napi::Env env, MD_SPANTYPE type, void *detail) {
	if (detail == nullptr) {
		return env.Null();
	}

	switch (type) {
		case MD_SPAN_A:
			return span_a_detail(env, static_cast<MD_SPAN_A_DETAIL *>(detail));
		case MD_SPAN_IMG:
			return span_img_detail(env, static_cast<MD_SPAN_IMG_DETAIL *>(detail));
		case MD_SPAN_WIKILINK:
			return span_wikilink_detail(env, static_cast<MD_SPAN_WIKILINK_DETAIL *>(detail));

		default:
			break;
	}

	return env.Null();
}

static int enter_block_callback(MD_BLOCKTYPE type, void *detail, void *userdata) {
	auto *data = static_cast<ParserData *>(userdata);

	data->enter_block.Call({
		Napi::Number::New(data->env, type),
		block_detail(data->env, type, detail)
	});

	return data->ret();
}

static int leave_block_callback(MD_BLOCKTYPE type, void *detail, void *userdata) {
	auto *data = static_cast<ParserData *>(userdata);

	data->leave_block.Call({
		Napi::Number::New(data->env, type),
		block_detail(data->env, type, detail)
	});

	return data->ret();
}

static int enter_span_callback(MD_SPANTYPE type, void *detail, void *userdata) {
	auto *data = static_cast<ParserData *>(userdata);

	data->enter_span.Call({
		Napi::Number::New(data->env, type),
		span_detail(data->env, type, detail)
	});

	return data->ret();
}

static int leave_span_callback(MD_SPANTYPE type, void *detail, void *userdata) {
	auto *data = static_cast<ParserData *>(userdata);

	data->leave_span.Call({
		Napi::Number::New(data->env, type),
		span_detail(data->env, type, detail)
	});

	return data->ret();
}

static int text_callback(MD_TEXTTYPE type, const MD_CHAR *text, MD_SIZE size, void *userdata) {
	auto *data = static_cast<ParserData *>(userdata);

	data->text.Call({
		Napi::Number::New(data->env, type),
		Napi::String::New(data->env, text, size)
	});

	return data->ret();
}

static void debug_log_callback(const char *msg, void* userdata) {
	auto *data = static_cast<ParserData *>(userdata);

	if (data->env.IsExceptionPending()) {
		return;
	}

	data->debug_log.Call({
		Napi::String::New(data->env, msg),
	});

	// Swallow the exception.
	if (data->env.IsExceptionPending()) {
		data->env.GetAndClearPendingException();
	}
}

void Parse(const Napi::CallbackInfo& info) {
	auto env = info.Env();

	CHECK_RETURN(info.Length() != 2, env, "Invalid number of arguments.");

	auto source_arg = info[0];
	auto options_arg = info[1];

	CHECK_TYPE_RETURN(String, source_arg, "source", env);
	CHECK_TYPE_RETURN(Object, options_arg, "options", env);

	auto options_obj = options_arg.As<Napi::Object>();

	MD_PARSER parser;
	ParserData data;

	parser.abi_version = 0;
	data.env = env;

	auto flags = options_obj.Get("flags");
	Napi::Number flags_num;
	UNWRAP_RETURN(flags, &flags_num);
	CHECK_TYPE_RETURN(Number, flags_num, "options.flags", env);

	parser.flags = flags_num.Uint32Value();

#define X(callback)															\
	auto callback = options_obj.Get(#callback);								\
	UNWRAP_RETURN(callback, &data.callback);								\
	CHECK_TYPE_RETURN(Function, data.callback, "options." #callback, env);

#define X_DEBUG_LOG(callback) \
	auto callback = options_obj.Get(#callback);								\
	UNWRAP_RETURN(callback, &data.callback);								\

CALLBACKS

#undef X
#undef X_DEBUG_LOG

	parser.enter_block = &enter_block_callback;
	parser.leave_block = &leave_block_callback;
	parser.enter_span = &enter_span_callback;
	parser.leave_span = &leave_span_callback;
	parser.text = &text_callback;
	parser.debug_log = (data.debug_log.IsUndefined()) ? nullptr : &debug_log_callback;
	parser.syntax = nullptr;

	auto source_str = source_arg.As<Napi::String>().Utf16Value();

	auto ret = md_parse(source_str.data(), source_str.size(), &parser, &data);

	CHECK_RETURN(ret != 0 && !env.IsExceptionPending(), env, "Parsing error.");
}

}
