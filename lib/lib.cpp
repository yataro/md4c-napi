#include <napi.h>

#include "constants.hpp"
#include "parse.hpp"
#include "html.hpp"



Napi::Object MD4CNapi_Init(Napi::Env env, Napi::Object exports) noexcept {
	exports.Set("constants", MD4CNapi::CreateConstantsObject(env));
	exports.Set("parse", Napi::Function::New(env, &MD4CNapi::Parse, "parse"));
	exports.Set("html", Napi::Function::New(env, &MD4CNapi::Html, "html"));

	return exports;
}

NODE_API_MODULE(md4c_napi, MD4CNapi_Init)
