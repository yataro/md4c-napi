# md4c-napi

nodejs N-API bindigns to md4c Markdown parsing library.

This library is a direct (as far as possible) binding to the md4c Markdown
library, providing a SAX-like interface to parsed Markdown.
This package also contains a binding to the md4c HTML converter, for
convenience.

## Examples

#### Parse markdown:
```js
const md4c = require('md4c-napi');

md4c.parse('**strong** `code`', {
	flags: md4c.constants.flag.DIALECT_COMMONMARK,
	enter_block(type, detail) {
		console.log('Entered block', type, detail);
	},
	leave_block(type, detail) {
		console.log('Leaved block', type, detail);
	},
	enter_span(type, detail) {
		console.log('Entered span', type, detail);
	},
	leave_span(type, detail) {
		console.log('Leaved span', type, detail);
	},
	text(type, text) {
		console.log('Got text', type, text);
	}
});
```

#### Convert Markdown to HTML:
```js
const md4c = require('md4c-napi');

md4c.html('**strong** `code`', {
	parser_flags: md4c.constants.flag.DIALECT_COMMONMARK,
	renderer_flags: 0,
	process_output(text) {
		console.log(text);
	}
});
```

## Author
[Aikawa Yataro](https://gitlab.com/yataro)
